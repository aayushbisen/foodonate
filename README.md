# Team koders
Hack 36 repository of team Koders.

## Problem

A platform where people can donate food to different food-donating organizations. 

## Proposed Solution

We are building an app to solve the problem of inefficient food usage in our country.