import 'package:shared_preferences/shared_preferences.dart';

String ngUrl(url) {
  //Usage:
  // ngUrl('/admin') => <ngrokurl>/admin

  String ngRokUrl = "http://1f3de5b4.ngrok.io";

  return "$ngRokUrl/$url";
}

void logoutDonor() async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.clear();
}

void logoutMediator() async {
  final prefs = await SharedPreferences.getInstance();
  await prefs.clear();
}


