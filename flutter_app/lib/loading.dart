import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Loading extends StatelessWidget {

  void _redirectIfNotAuthenticated(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();

    if (await prefs.containsKey("token")) {
      bool containsIsDonor = await prefs.containsKey("isDonor");
      bool containsIsMediator = await prefs.containsKey("isMediator");

      if (containsIsDonor) {
        if (await prefs.getBool("isDonor")) {
          Navigator.of(context).pushReplacementNamed('/donor-home');
        }
      } else if (containsIsMediator) {
        if (await prefs.getBool("isMediator")) {
          Navigator.of(context).pushReplacementNamed('/mediator-home');
        }
      } else {
        Navigator.of(context).pushReplacementNamed('/login-donor');

      }
    } else {
        Navigator.of(context).pushReplacementNamed('/login-donor');
    }
  }

  @override
  Widget build(BuildContext context) {

    _redirectIfNotAuthenticated(context);

    return Container(
      child: Center(
          child: CircularProgressIndicator(
        value: null,
      )),
    );
  }
}
