import 'package:flutter_app/loading.dart';
import 'package:flutter_app/pages/EditUserProfile.dart';
import 'package:flutter_app/pages/createEvent.dart';
import 'package:flutter_app/pages/donorHome.dart';
import 'package:flutter_app/pages/eventDetail.dart';
import 'package:flutter_app/pages/eventList.dart';
import 'package:flutter_app/pages/login.dart';
import 'package:flutter_app/pages/loginMediator.dart';
import 'package:flutter_app/pages/mediatorHome.dart';
import 'package:flutter_app/pages/signup.dart';
import 'package:flutter_app/pages/signupMediator.dart';
import 'package:flutter/material.dart';


void main() => runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      theme: ThemeData(
        buttonColor: Colors.lightBlue,
        primaryColor: Colors.blueAccent,
        accentColor: Colors.deepPurpleAccent,
      ),
      routes: {
        '/': (context) => LoginDonor(),
        '/mediator-home': (context) => MediatorHome(),
        '/donor-home': (context) => DonorHome(),
        '/signup-donor': (context) => SignupDonor(),
        '/login-donor': (context) => LoginDonor(),
        '/login-mediator': (context) => LoginMediator(),
        '/signup-mediator': (context) => SignupMediator(),
        '/create-event': (context) => CreateEvent(),
        '/edit-profile': (context) => UserProfile(),
        '/event-detail': (context) => EventDetail(),
      },
    ));
