import 'package:flutter/material.dart';
import 'package:flutter_app/helpers.dart';
import 'package:flutter_app/pages/apiUrls.dart';
import 'package:flutter_app/pages/eventList.dart';

class MediatorHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EventList(
        title: "Events nearby",
        eventsURL: ngUrl(MEDIATOR_EVENTS),
        token: "9c9abe8cf7a847c6330065cad6b08500b5781fd8",
        showFab: false,
      );
  }
}
