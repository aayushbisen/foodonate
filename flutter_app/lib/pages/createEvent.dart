import 'package:flutter_app/helpers.dart';
import 'package:flutter_app/pages/apiUrls.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CreateEvent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CreateEventState();
  }
}

class CreateEventState extends State<CreateEvent> {
  String _address;
  String _eventName;
  String _food;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void createEvent() async {
    final prefs = await SharedPreferences.getInstance();
    String createEventUrl = ngUrl(CREATE_EVENT_URL);
    String donorId = "6";

    var response = await http.post(createEventUrl, body: {
      'donorId': donorId,
      'name': _eventName,
      'food_waste_quantity': _food,
      'address': _address
    }, headers: {"Authorization": "Token d58cee4f0be0262456b90c13990af189b9ff4031"});

    Map resData = jsonDecode(response.body);

    print("#RESPONSE@createEvent.dart: $resData");

    Navigator.pop(context);
  }

  Widget _buildLabel() {
    return Text(
      'Make a Donation',
      style: TextStyle(fontSize: 28, color: Colors.grey[900]),
    );
  }

  Widget _buildEvent() {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: ' Event Name',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Event Name is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _eventName = value;
      },
    );
  }

  Widget _buildAddress() {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Address',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Address is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _address = value;
      },
    );
  }

  Widget _buildFood() {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Food Quantity(in kg)',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Food quantity is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _food = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Food Donation')),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              _buildLabel(),
              SizedBox(height: 15,),
              _buildEvent(),
              SizedBox(height: 15,),
              _buildFood(),
              SizedBox(
                height: 15,
              ),
              _buildAddress(),
              SizedBox(height: 10),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.grey)),
                color: Color(0xff01A0C7),
                elevation: 5.0,
                child: Text(
                  'Donate',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_formKey.currentState.validate()) {
                    return;
                  }
                  _formKey.currentState.save();
                  createEvent();
                  //Send to API
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
