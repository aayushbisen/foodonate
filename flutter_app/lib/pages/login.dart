import 'package:flutter_app/helpers.dart';
import 'package:flutter_app/pages/apiUrls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class LoginDonor extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginDonorState();
  }
}

class LoginDonorState extends State<LoginDonor> {
  String _phoneNumber;
  String _password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void loginDonor() async {
    String loginUrl = ngUrl(DONOR_LOGIN_URL);

    var response = await http.post(loginUrl,
        body: {'phone_number': _phoneNumber, 'password': _password});

    var responseData = jsonDecode(response.body);

    final prefs = await SharedPreferences.getInstance();

    await prefs.clear();
    await prefs.setString("token", responseData['token']);
    await prefs.setBool("isDonor", true);
    await prefs.setBool("isMediator", false);
    await prefs.setInt("donorId", responseData['donorId']);
    await prefs.setString("donorName", responseData['donorName']);

    Navigator.of(context).pushReplacementNamed('/donor-home');
  }

  Widget _buildLabel() {
    return Text(
      'Login as Donor',
      style: TextStyle(fontSize: 28.0, color: Colors.grey[900]),
    );
  }

//   Widget _buildEmail() {
//     return TextFormField(
//       decoration: InputDecoration(
//           contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
//           labelText: 'Email',
//           border:
//               OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
//       validator: (String value) {
//         if (value.isEmpty) {
//           return 'Email is Required';
//         }

//         if (!RegExp(
//                 r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
//             .hasMatch(value)) {
//           return 'Please enter a valid email Address';
//         }

//         return null;
//       },
//       onSaved: (String value) {
//         _email = value;
//       },
//     );
//   }

  Widget _buildPhoneNumber() {
    return TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Phone number',
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      keyboardType: TextInputType.phone,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Phone number is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _phoneNumber = value;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Password',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      keyboardType: TextInputType.visiblePassword,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Password is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _password = value;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Food Donation')),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                child: _buildLabel(),
              ),
              SizedBox(
                height: 15,
              ),
              _buildPhoneNumber(),
              SizedBox(
                height: 15,
              ),
              _buildPassword(),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.grey)),
                    elevation: 5.0,
                    child: Text(
                      'Login',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }

                      _formKey.currentState.save();

                      loginDonor();

                      //Send to API
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(' or '),
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.grey)),
                    elevation: 5.0,
                    child: Text(
                      'Signup',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () {
                     Navigator.of(context).pushNamed("/signup-donor");
                      
                    },
                  ),
                ],
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed("/login-mediator");
                },
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.grey)),
                child: Text(
                  "Login as mediator",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
