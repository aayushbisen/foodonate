import 'package:flutter/material.dart';
import 'package:flutter_app/helpers.dart';
import 'package:flutter_app/pages/apiUrls.dart';
import 'package:flutter_app/pages/eventList.dart';

class DonorHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return EventList(
        title: "My Events",
        eventsURL: ngUrl(DONOR_EVENTS),
        token: "d58cee4f0be0262456b90c13990af189b9ff4031", showFab: true,); // Adonor
  }
}
