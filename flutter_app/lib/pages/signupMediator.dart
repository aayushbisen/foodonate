import 'package:flutter_app/helpers.dart';
import 'package:flutter_app/pages/apiUrls.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class SignupMediator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignupMediatorState();
  }
}

class SignupMediatorState extends State<SignupMediator> {

  String _name;
  String _address;
  String _password;
  String _phoneNumber;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void signupMediator() async {
    String signupUrl = ngUrl(MEDIATOR_SIGNUP_URL);

    var response = await http.post(signupUrl, body: {
      'name': _name,
      'phone_number': _phoneNumber,
      'password': _password,
      'address': _address
    });

    var responseData = jsonDecode(response.body);

    final prefs = await SharedPreferences.getInstance();

    await prefs.clear();
    await prefs.setString("token", responseData['token']);
    await prefs.setBool("isMediator", true);
    await prefs.setBool("isDonor", false);
    await prefs.setInt("mediatorId", responseData['mediatorId']);
    await prefs.setString("mediatorName", responseData['mediatorName']);

    Navigator.of(context).pushReplacementNamed('/mediator-home');
  }

  Widget _buildLabel() {
    return Text(
      'Signup as Mediator',
      style: TextStyle(fontSize: 28, color: Colors.grey[900]),
    );
  }

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Name',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Name is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _name = value;
      },
    );
  }

  Widget _buildAddress() {
    return TextFormField(
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Address',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      validator: (String value) {
        if (value.isEmpty) {
          return 'Address is required';
        }

        return null;
      },
      onSaved: (String value) {
        _address = value;
      },
    );
  }

  // Widget _buildEmail() {
  //   return TextFormField(
  //     decoration: InputDecoration(
  //         contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
  //         labelText: 'Email',
  //         border:
  //             OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  //     validator: (String value) {
  //       if (value.isEmpty) {
  //         return 'Email is Required';
  //       }

  //       if (!RegExp(
  //               r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
  //           .hasMatch(value)) {
  //         return 'Please enter a valid email Address';
  //       }

  //       return null;
  //     },
  //     onSaved: (String value) {
  //       _email = value;
  //     },
  //   );
  // }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: 'Password',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
      keyboardType: TextInputType.visiblePassword,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Password is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _password = value;
      },
    );
  }

  Widget _buildPhoneNumber() {
    return TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          labelText: 'Phone number',
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      keyboardType: TextInputType.phone,
      validator: (String value) {
        if (value.isEmpty) {
          return 'Phone number is Required';
        }

        return null;
      },
      onSaved: (String value) {
        _phoneNumber = value;
      },
    );
  }

  Widget _buildId() {
    return TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          labelText: 'Uniquie Id',
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
      keyboardType: TextInputType.phone,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Food Donation')),
      body: Container(
        margin: EdgeInsets.all(24),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                child: _buildLabel(),
              ),
              SizedBox(
                height: 10,
              ),
              _buildName(),
              // _buildEmail(),
              SizedBox(
                height: 10,
              ),
              _buildPassword(),
              SizedBox(
                height: 10,
              ),
              _buildPhoneNumber(),
              SizedBox(
                height: 10,
              ),
              _buildAddress(),
              SizedBox(height: 40),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(18.0),
                    side: BorderSide(color: Colors.grey)),
                color: Color(0xff01A0C7),
                elevation: 5.0,
                child: Text(
                  'Submit',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  if (!_formKey.currentState.validate()) {
                    return;
                  }

                  _formKey.currentState.save();

                  signupMediator();

                  //Send to API
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
