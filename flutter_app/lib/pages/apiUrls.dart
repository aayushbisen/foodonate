String DONOR_LOGIN_URL = "api/donor-login";
String MEDIATOR_LOGIN_URL = "api/mediator-login";
String DONOR_SIGNUP_URL = "api/donor-signup";
String MEDIATOR_SIGNUP_URL = "api/mediator-signup";
String CREATE_EVENT_URL = "api/create-event";
String DONOR_EVENTS = "api/events-donor";
String MEDIATOR_EVENTS = "api/events-mediator";
