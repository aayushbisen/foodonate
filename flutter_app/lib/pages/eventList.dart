import 'dart:async';
import 'dart:convert';

import 'package:flutter_app/helpers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/pages/eventDetail.dart';
import 'package:http/http.dart' as http;
import 'dart:io';

class Event {
  final String name;
  final String donor;
  final String address;
  final int food_quantity;

  Event({this.name, this.donor, this.address, this.food_quantity});

  factory Event.fromJson(Map<String, dynamic> json) {
    return Event(
      name: json['name'],
      address: json['address'],
    );
  }
}

class EventList extends StatefulWidget {
  final String title;
  final String eventsURL;
  final String token;
  final bool showFab;
  EventList({Key key, this.title, this.eventsURL, this.token, this.showFab})
      : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  Future<List<Event>> fetchEvents(String url, String token) async {
    final response =
        await http.get(url, headers: {"Authorization": "Token $token"});

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response, then parse the JSON.
      List jsonResponse = json.decode(response.body);
      return jsonResponse.map((event) => new Event.fromJson(event)).toList();
    } else {
      // If the server did not return a 200 OK response, then throw an exception.
      print("Failed to load events");
    }
  }

  Widget _jobsListView(data) {
    return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return _tile(data[index].name, data[index].address, Icons.event);
        });
  }

  ListTile _tile(String title, String venue, IconData icon) => ListTile(
        onTap: () {
          // Navigator.of(context).pushNamed('/event-detail');

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EventDetail(
                title: title,
                venue: venue,
              ),
            ),
          );
        },
        title: Text(title,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
            )),
        subtitle: Text(venue),
        leading: Icon(
          icon,
          color: Colors.blue[500],
        ),
      );

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.person),
            onPressed: () {

              Widget userInfo = widget.showFab ? Text("Loggined as Duck") : Text("Loggined as The NGO");

              showDialog(context: context, child: AlertDialog(title: userInfo, content: Text("Click the icon on top right to logout."),));
            },
          ),
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              logoutDonor();
              logoutMediator();
              Navigator.of(context).pushReplacementNamed('/login-donor');
            },
          ),
        ],
      ),
      body: FutureBuilder<List<Event>>(
        future: fetchEvents(widget.eventsURL, widget.token),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Event> data = snapshot.data;
            return _jobsListView(data);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          return Center(
            child: Container(
              child: Text("Loading ..."),
            ),
          );
        },
      ),
      floatingActionButton: Visibility(
        visible: widget.showFab,
        child: FloatingActionButton(
          backgroundColor: Colors.lightBlue,
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).pushNamed('/create-event');
          },
        ),
      ),
    );
  }
}
