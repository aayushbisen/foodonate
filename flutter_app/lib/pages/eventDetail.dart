import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';

class EventDetail extends StatefulWidget {

  final String title;
  final String venue;

  EventDetail({
    Key key,
    this.title,
    this.venue,
  }) : super(key: key);

  @override
  _EventDetailState createState() => _EventDetailState();

}

class _EventDetailState extends State<EventDetail> {
  
  void getAddress(address) {
    MapsLauncher.launchQuery(address);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Detail'),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 5,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.event, size: 40,),
                       SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        widget.title,
                        style: TextStyle(
                            fontSize: 30.0, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        widget.venue,
                        style: TextStyle(
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: RaisedButton.icon(
                    color: Colors.green,
                    icon: Icon(
                      Icons.map,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      getAddress(widget.venue);
                    },
                    label: Text(
                      'Open in Maps',
                      style: TextStyle(fontSize: 20.0, color: Colors.white),
                    )),
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(17.0),
                child: RaisedButton.icon(
                    onPressed: () {
                      showDialog(
                          context: context,
                          child: AlertDialog(
                            title: Text("Food donation confirmed"),
                            content: Text(
                                "Contact Bhilai Club at this phone number: 7894561230"),
                          ));
                    },
                    // color: Theme.of(context).primaryColor,
                    icon: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                    label: Text(
                      'Confirm Pickup',
                      style: TextStyle(
                        fontSize: 20.0,
                        color: Colors.white,
                      ),
                    )),
              ),
            ),
          ],
        ));
  }
}
