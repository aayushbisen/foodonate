import 'package:flutter/material.dart';

void main() =>
    runApp(MaterialApp(debugShowCheckedModeBanner: false, home: HomePage()));

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 400,
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: -40,
                  height: 400,
                  width: width,
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/background.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                Positioned(
                  height: 400,
                  width: width + 20,
                  child: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage("assets/images/background-2.png"),
                            fit: BoxFit.fill)),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Center(
                    child: Text(
                  "Food Donation",
                  style: TextStyle(
                      color: Color.fromRGBO(49, 39, 79, 0.9),
                      fontWeight: FontWeight.bold,
                      fontSize: 35),
                )),
                SizedBox(
                  height: 15,
                ),
                Center(
                    child: Text(
                  "lend a helping hand",
                  style: TextStyle(
                      color: Color.fromRGBO(49, 39, 79, 0.5),
                      fontWeight: FontWeight.normal,
                      fontSize: 18),
                )),
                SizedBox(
                  height: 50,
                ),
                Center(
                  child: RaisedButton(
                    padding: EdgeInsets.all(15),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0),
                        side: BorderSide(color: Colors.grey)),
                    color: Color.fromRGBO(49, 39, 79, 1),
                    elevation: 5.0,
                    child: Text(
                      'Login as Donor',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }

                      _formKey.currentState.save();
                      //Send to API
                    },
                  ),
                ),
                SizedBox(height: 10,),
                Center(child: Text('or',style: TextStyle(fontSize: 20, fontStyle: FontStyle.italic))),
                SizedBox(height: 10,),
                Center(
                  child: RaisedButton(
                    padding: EdgeInsets.all(14),
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(50.0),
                        side: BorderSide(color: Colors.grey)),
                    color: Color.fromRGBO(49, 39, 79, 1),
                    elevation: 5.0,
                    child: Text(
                      'Login as Volunteer',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }

                      _formKey.currentState.save();
                      //Send to API
                    },
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
//color: Color.fromRGBO(49, 39, 79, 1),
