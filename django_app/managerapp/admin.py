from django.contrib import admin
from .models import Mediator, Donor, Event
# Register your models here.


admin.site.register(Mediator)
admin.site.register(Donor)
admin.site.register(Event)
