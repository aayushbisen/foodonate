from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from .validators import validate_phone_number

# Create your models here.


# class Location(models.Model):

#     longitude = models.FloatField()
#     latitude = models.FloatField()



class Mediator(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    # location = models.ForeignKey(Location, default=None, blank=True, null=True)
    address = models.TextField(max_length=2000)
    phone_number = models.PositiveIntegerField(
        "Phone number", validators=[validate_phone_number], unique=True)

    def __str__(self):
        return f"Mediator({self.user})"

    def events(self):
        return Event.objects.filter(
            mediator__user__id=self.user.pk,
        ).order_by('-date_time_created')


class Donor(models.Model):

    user = models.OneToOneField(
        User, on_delete=models.CASCADE, primary_key=True)
    # location = models.ForeignKey(Location, default=None, blank=True, null=True)
    address = models.TextField(
        max_length=2000, default=None, blank=True, null=True)
    phone_number = models.PositiveIntegerField(
        "Phone number", validators=[validate_phone_number], unique=True)

    def __str__(self):
        return f"Donor({self.user})"

    def events(self):
        return Event.objects.filter(
            donor__user__id=self.user.id,
        ).order_by('-date_time_created')


class Event(models.Model):

    donor = models.ForeignKey(Donor, on_delete=models.CASCADE)
    mediator = models.ForeignKey(
        Mediator, on_delete=models.CASCADE, default=None, blank=True, null=True)
    name = models.CharField(max_length=100)
    food_waste_quantity = models.PositiveIntegerField()
    address = models.TextField(max_length=2000)
    taken = models.BooleanField(default=False)
    food_donated = models.BooleanField(default=False)
    date_time_created = models.DateTimeField("Created", auto_now=False, auto_now_add=True)

    def __str__(self):
        return f"Event({self.name})"

    def confirm_pickup(self, mediator):
        """ Mediator clicks the 'confirm pickup' button """
        self.mediator = mediator
        self.taken = True
        self.save()

    def confirm_food_donation(self, mediator):

        if self.mediator == mediator:
            self.food_donated = True
            self.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
