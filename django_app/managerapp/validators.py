from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_phone_number(number):

    str_num = str(number)
    error_msg = 'Not a valid phone number'

    if len(str_num) != 10:
        raise ValidationError(error_msg)

    if not str_num.isdigit():
        raise ValidationError(error_msg)


def is_valid_phone_number(number):
    
    return len(str(number)) == 10 and str(number).isdigit()