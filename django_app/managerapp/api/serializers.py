from managerapp.models import Mediator, Donor, Event
from rest_framework import serializers
from django.contrib.auth.models import User

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ['donor', 'mediator', 'name', 'food_waste_quantity', 'address', 'taken', 'food_donated']
        depth = 3
        

class MediatorSerializer(serializers.ModelSerializer):


    class Meta:
        model = Mediator
        fields = ['user', 'address', 'phone_number']
        depth = 1

class DonorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Donor
        fields = ['user', 'address', 'phone_number']
        depth = 3

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name']
        depth = 1