from rest_framework import permissions
from managerapp.models import Mediator, Donor

class IsMediatorAuthenticated(permissions.IsAuthenticated):
    
    def has_permissions(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        user = request.user
        return bool(request.user and request.user.is_authenticated and Mediator.objects.fliter(user=user))


class IsDonorAuthenticated(permissions.IsAuthenticated):
    
    def has_permissions(self, request, view):
        if request.method in permissions.SAFE_METHODS:
            return True

        user = request.user
        return bool(request.user and request.user.is_authenticated and Donor.objects.fliter(user=user))
