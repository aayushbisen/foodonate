from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.response import Response
from uuid import uuid4
from rest_framework.authentication import TokenAuthentication
from django.db.models import Model
from managerapp.models import Donor, Event, Mediator
from managerapp.validators import validate_phone_number, is_valid_phone_number

from . import permissions as custom_permissions
from .serializers import DonorSerializer, EventSerializer, MediatorSerializer, UserSerializer


class MediatorViewSet(viewsets.ModelViewSet):
    queryset = Mediator.objects.all()
    serializer_class = MediatorSerializer


class DonorViewSet(viewsets.ModelViewSet):
    permission_classes = [custom_permissions.IsDonorAuthenticated]
    queryset = Donor.objects.all()
    serializer_class = DonorSerializer


# class EventViewSet(viewsets.ModelViewSet):

#     # permission_classes = [custom_permissions.IsMediatorAuthenticated]

#     queryset = Event.objects.filter(taken=False)
#     serializer_class = EventSerializer


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([custom_permissions.IsDonorAuthenticated])
def fetch_events_donor(request):
    donor = Donor.objects.get(user=request.user)
    events_donor = donor.events()
    data = EventSerializer(events_donor, many=True).data
    for i, event in enumerate(data):
        user_pk = event['donor']['user']
        user = User.objects.get(pk=user_pk)
        user_data = UserSerializer(user).data
        data[i]['donor']['user'] = user_data
    return Response(data)


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([custom_permissions.IsMediatorAuthenticated])
def fetch_events_mediator(request):
    mediator = Mediator.objects.get(user=request.user)
    events_mediator = Event.objects.all().order_by('-date_time_created')
    data = EventSerializer(events_mediator, many=True).data
    # for i, event in enumerate(data):
    #     donor_user_pk = event['donor']['user']
    #     mediator_user_pk = event['mediator']['user']

    #     donor_user = User.objects.get(pk=donor_user_pk)
    #     mediator_user = User.objects.get(pk=mediator_user_pk)

    #     donor_user_data = UserSerializer(donor_user).data
    #     mediator_user_data = UserSerializer(mediator_user).data

    #     data[i]['donor']['user'] = donor_user_data
    #     data[i]['mediator']['user'] = mediator_user_data
    return Response(data)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([custom_permissions.IsMediatorAuthenticated])
def confirm_pickup(request):

    event_id = request.data['event_id']
    try:
        mediator = Mediator.objects.get(user__id=request.user.id)
        event = Event.objects.get(id=event_id)

        event.confirm_pickup(mediator)

        return Response({
            'event': EventSerializer(event).data
        })

    except Model.DoesNotExist:
        return Response({
            "error": "No mediator found with this username."
        })


@api_view(["GET"])
@authentication_classes([TokenAuthentication])
@permission_classes([custom_permissions.IsDonorAuthenticated])
def fetch_donor_details(request):
    donor = Donor.objects.get(user__id=request.user.id)
    donor_dict = DonorSerializer(donor).data
    user_dict = UserSerializer(request.user).data
    final_dict = {**donor_dict, **user_dict}

    return Response(final_dict)


@api_view(["POST"])
@authentication_classes([TokenAuthentication])
@permission_classes([custom_permissions.IsDonorAuthenticated])
def create_event(request):

    name = request.data['name']
    address = request.data['address']
    food_waste_quantity = request.data['food_waste_quantity']

    try:

        donor = Donor.objects.get(user__id=request.user.id)
        event = Event.objects.create(
            name=name,
            donor=donor,
            food_waste_quantity=food_waste_quantity,
            address=address,
        )

        event.save()

        return Response({
            'event': EventSerializer(event).data,
        })

    except Event.DoesNotExist:
        return Response({
            'error': 'Cannot find donor attached to this user'
        })


@api_view(['POST'])
def mediator_login(request):

    data = request.data
    err_msg = "Invalid phone number and password"

    try:
        try:
            validate_phone_number(data['phone_number'])
            mediator = Mediator.objects.get(phone_number=data['phone_number'])

            if check_password(data['password'], mediator.user.password):
                token = Token.objects.get(user=mediator.user)

                tokenDict = {
                    'token': token.key,
                    'mediatorId': mediator.pk,
                    'mediatorName': mediator.user.first_name,
                }
                return Response(tokenDict)

            else:
                return Response({
                    'error': err_msg
                })

        except ValidationError as e:
            return Response({
                'error': err_msg
            })

    except Mediator.DoesNotExist:
        return Response({
            'error': err_msg
        })


@api_view(['POST'])
def donor_login(request):

    data = request.data
    err_msg = "Invalid phone number and password"

    try:
        try:
            validate_phone_number(data['phone_number'])
            donor = Donor.objects.get(phone_number=data['phone_number'])

            if check_password(data['password'], donor.user.password):
                print(donor.user)
                token = Token.objects.get(user=donor.user)

                tokenDict = {
                    'token': token.key,
                    'donorId': donor.pk,
                    'donorName': donor.user.first_name,
                }
                return Response(tokenDict)

            else:
                return Response({
                    'error': err_msg
                })

        except ValidationError:
            return Response({
                'error': err_msg
            })

    except Donor.DoesNotExist:
        return Response({
            'error': err_msg
        })


@api_view(["POST"])
def mediator_signup(request):

    data = request.data

    try:
        validate_phone_number(data['phone_number'])
        if not Mediator.objects.filter(phone_number=data['phone_number']).count():
            user = User.objects.create_user(
                str(uuid4()), password=make_password(data['password']))
            user.first_name = data['name']
            user.save()
            mediator = Mediator.objects.create(
                user=user, address=data['address'], phone_number=data['phone_number'])
            mediator.save()
            token = Token.objects.get(user=user).key
            return Response({
                'token': token,
                'mediatorId': mediator.pk,
                'mediatorName': mediator.user.first_name
            })
        else:
            return Response({
                'error': 'Mediator Already Exist!'
            })

    except ValidationError as e:

        return Response({
            'error': e.message
        })


@api_view(["POST"])
def donor_signup(request):

    data = request.data

    try:
        validate_phone_number(data['phone_number'])
        if not Donor.objects.filter(phone_number=data['phone_number']).count():
            user = User.objects.create_user(
                str(uuid4()), password=make_password(data['password']))
            user.first_name = data['name']
            user.save()
            donor = Donor.objects.create(
                user=user, address=data['address'], phone_number=data['phone_number'])
            donor.save()
            token = Token.objects.get(user=user).key
            return Response({
                'token': token,
                'donorId': donor.pk,
                'donorName': donor.user.first_name,
            })
        else:
            return Response({
                'error': 'Donor Already Exist!'
            })

    except ValidationError as e:
        return Response({
            'error': e.message
        })
