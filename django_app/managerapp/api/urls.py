from django.urls import include, path
from rest_framework import routers
from .import views

router = routers.DefaultRouter()
router.register('donors', views.DonorViewSet)
router.register('mediators', views.MediatorViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('donor-login', views.donor_login),
    path('mediator-login', views.donor_login),
    path('mediator-signup', views.mediator_signup),
    path('donor-signup', views.donor_signup),
    path('events-donor', views.fetch_events_donor),
    path('events-mediator', views.fetch_events_mediator),
    path('fetch-donor', views.fetch_donor_details),
    path('create-event', views.create_event),
    path('confirm-pickup', views.confirm_pickup),
]
